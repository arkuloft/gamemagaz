<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SampleController extends Controller
{
    public function singlenews()
    {
        return view('pages.news.single');
    }
    public function listnews()
    {
        return view('pages.news.list');
    }
    public function about()
    {
        return view('pages.about');
    }

    public function singleproduct()
    {
        return view('pages.product.single');
    }
    public function category()
    {
        return view('pages.category.category');
    }
    public function cartstep1()
    {
        return view('pages.cart.step1');
    }
    public function cartstep2()
    {
        return view('pages.cart.step2');
    }
    public function cartstep3()
    {
        return view('pages.cart.step3');
    }
}
